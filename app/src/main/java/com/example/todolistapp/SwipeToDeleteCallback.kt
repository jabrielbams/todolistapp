package com.example.todolistapp

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar

class SwipeToDeleteCallback(private val viewModel: TaskViewModel) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        // Do nothing
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        val taskItem = viewModel.taskItems.value?.get(position)

        taskItem?.let {
            viewModel.deleteTaskItem(it)
        }

        Snackbar.make(viewHolder.itemView, "Task Di Hapus", Snackbar.LENGTH_LONG)
            .setAction("Batal") {
                taskItem?.let {
                    viewModel.addTaskItem(it)
                }
            }
            .show()
    }
}
