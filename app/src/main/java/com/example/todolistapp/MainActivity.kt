package com.example.todolistapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todolistapp.databinding.ActivityMainBinding
import com.example.todolistapp.room.TaskItem

class MainActivity : AppCompatActivity(), TaskItemClickListener
{
    private lateinit var binding: ActivityMainBinding
    private val taskViewModel: TaskViewModel by viewModels {
        TaskItemModelFactory((application as ToDoApplication).repository)
    }
    private lateinit var taskAdapter: TaskItemAdapter

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.newTaskButton.setOnClickListener {
            NewTaskSheetFragment(null).show(supportFragmentManager, "newTaskTag")
        }
        setRecyclerView()
        setSearchBar()

        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(taskViewModel))
        itemTouchHelper.attachToRecyclerView(binding.todoListRecyclerView)
    }

    private fun setRecyclerView() {
        taskAdapter = TaskItemAdapter(emptyList(), this)
        binding.todoListRecyclerView.apply {
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = taskAdapter
        }
        taskViewModel.taskItems.observe(this) { taskList ->
            taskAdapter.updateList(taskList)
        }
    }

    private fun setSearchBar() {
        binding.etSearch.doAfterTextChanged { query ->
            taskViewModel.searchTaskItems(query.toString()).observe(this) { taskList ->
                taskAdapter.updateList(taskList)
            }
        }
    }

    override fun editTaskItem(taskItem: TaskItem)
    {
        NewTaskSheetFragment(taskItem).show(supportFragmentManager,"newTaskTag")
    }

    override fun completeTaskItem(taskItem: TaskItem)
    {
        taskViewModel.setCompleted(taskItem)
    }

}