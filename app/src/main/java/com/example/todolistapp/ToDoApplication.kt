package com.example.todolistapp

import android.app.Application
import com.example.todolistapp.room.TaskItemDatabase
import com.example.todolistapp.room.TaskItemRepository

class ToDoApplication : Application()
{
    private val database by lazy { TaskItemDatabase.getDatabase(this) }
    val repository by lazy { TaskItemRepository(database.taskItemDao()) }
}